
# sex stable
drop table if exists sex;
create table sex(
  id int primary key auto_increment comment 'sex id',

  flag char(4) unique not null comment 'sex name'
);

# user table
drop table if exists user;
create table user(
    uuid char(129) primary key comment 'user primary key of uuid',

    name nvarchar(64) comment 'user name',

    sex int(1) not null comment 'user sex',

    account nvarchar(32) not null comment 'user account',

    emil nvarchar(64) comment 'user bound mailbox',

    photo nvarchar(255) not null default 'default' comment 'user photo',

    age int(3) comment 'user age',

    personalized_signature_id int ,

    create_time char(19) not null comment 'user account create time',

    foreign key(sex) references sex(id)
);

# create an index for account filed in the user table.
create index user_account_index on user(account);

# personalized signature table.
drop table if exists personalized_signature;
create table personalized_signature(
    ps_id int primary key comment 'personalized signature id' auto_increment,

    content nvarchar(2048) comment 'personalized signature content',

    disable bit default 0 comment 'disable or not',

    create_time char(19) not null comment 'personalized signature create time',

    disable_time char(19) comment 'personalized signature disable time',

    user_id char(129) comment 'user primary key of uuid',

    foreign key (user_id) references user(uuid)
);


drop table if exists article_tag;

create table article_tag(
  tag_id int primary key auto_increment comment 'tag id',

  tag_name nvarchar(32) not null comment 'tag name',

  create_time char(19) not null comment 'tag create time',

  remove bit default 0 not null comment 'tag remove or no',

  remove_time char(19) comment 'remove time'
);

create table article_comment_state(
    acs_id int primary key auto_increment comment 'article comment state id',

    disable bit not null default 0 comment 'is the comment disable',

    reason nvarchar(1024) not null comment 'disable reason',

    complete_reason nvarchar(2048) not null comment 'complete reason of disable',

    article_comment_id int not null comment 'article comment id',

    foreign key (article_comment_id) references article_comment(ac_id)
);

create table article_comment(
    ac_id int primary key auto_increment comment 'article comment id',

    reply_id int comment 'reply to user..',

    content nvarchar(2048) not null comment 'user comment',

    create_time char(19) not null comment 'comment create time',

    article_comment_state_id int comment 'comment state',

    foreign key(reply_id) references article_comment(ac_id)
);